# Install Vagrant

    sudo su
    apt-get update && apt-get install -y libvirt-dev ruby-all-dev apparmor-utils
    curl -O -L https://dl.bintray.com/mitchellh/vagrant/vagrant_1.6.5_x86_64.deb
    dpkg -i vagrant_1.6.5_x86_64.deb 
    aa-complain /usr/lib/libvirt/virt-aa-helper # workaround
    exit

## Install vagrant-kvm as user

    vagrant plugin install vagrant-kvm 

# Config kvm

    sudo vim /etc/modprobe.d/kvm.conf
    cat /etc/modprobe.d/kvm.conf
        options kvm-intel nested=1
        options kvm-amd   nested=1

# Add ubuntu trusty image

    vagrant box add trusty64 https://vagrant-kvm-boxes-si.s3.amazonaws.com/trusty64-kvm-20140418.box
    vagrant box list

# Create Vagrantfile

    mkdir -p ~/vagrant/test
    cd ~/vagrant/test
    vagrant init trusty64
    
## Config Vagrantfile

    Vagrant.configure('2') do |config|
      config.vm.box = "trusty64"

      config.vm.network :public_network, ip: "192.168.11.197"
      config.vm.network :private_network, ip: "192.168.123.10"
      config.vm.provider :kvm do |kvm, override|
        kvm.memory_size     = '2048m'
      end
    end

# Vagrant up

    vagrant up --provider=kvm

## Login to VM

    vagrant ssh

# Multi-node Vagrantfile example

    Vagrant.configure('2') do |config|
      config.vm.box = "trusty64"
      
      config.vm.define :control do |control|
        control.vm.hostname = "controller"
        control.vm.network :public_network
        control.vm.network :private_network, ip: "192.168.123.10"
        control.vm.provider :kvm do |kvm, override|
          kvm.memory_size     = '2048m'
        end
      end

      config.vm.define :network do |network|
        network.vm.hostname = "network"
        network.vm.network :public_network
        network.vm.network :private_network, ip: "192.168.123.12"
        network.vm.provider :kvm do |kvm, override|
          kvm.memory_size     = '2048m'
        end
      end

    end

## Access to node

    vagrant ssh control
    vagrant ssh network