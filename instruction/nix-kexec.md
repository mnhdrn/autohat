sh <(curl -L https://nixos.org/nix/install) --daemon

 

 

tee kexec.nix << "EOF"
{ config, pkgs, ... }:
{
  imports = [
    # this will work only under qemu, uncomment next line for full image
    # <nixpkgs/nixos/modules/installer/netboot/netboot-minimal.nix>
<nixpkgs/nixos/modules/installer/netboot/netboot.nix>
<nixpkgs/nixos/modules/profiles/qemu-guest.nix>
  ];

 

  # stripped down version of https://github.com/cleverca22/nix-tests/tree/master/kexec
  system.build = rec {
    image = pkgs.runCommand "image" { buildInputs = [ pkgs.nukeReferences ]; } ''
      mkdir $out
      cp ${config.system.build.kernel}/${config.system.boot.loader.kernelFile} $out/kernel
      cp ${config.system.build.netbootRamdisk}/initrd $out/initrd
      nuke-refs $out/kernel
    '';
    kexec_script = pkgs.writeTextFile {
      executable = true;
      name = "kexec-nixos";
      text = ''
        #!${pkgs.stdenv.shell}
        set -e
        ${pkgs.kexectools}/bin/kexec -l ${image}/kernel --initrd=${image}/initrd --append="init=${builtins.unsafeDiscardStringContext config.system.build.toplevel}/init ${toString config.boot.kernelParams}"
        sync
        echo "executing kernel, filesystems will be improperly umounted"
        ${pkgs.kexectools}/bin/kexec -e
      '';
    };
    kexec_tarball = pkgs.callPackage <nixpkgs/nixos/lib/make-system-tarball.nix> {
      storeContents = [
        {
          object = config.system.build.kexec_script;
          symlink = "/kexec_nixos";
        }
      ];
      contents = [ ];
      compressCommand = "cat";
      compressionExtension = "";
    };
    kexec_tarball_self_extract_script = pkgs.writeTextFile {
      executable = true;
      name = "kexec-nixos";
      text = ''
        #!/bin/sh
        set -eu
        ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ { print NR + 1; exit 0; }' $0`
        tail -n+$ARCHIVE $0 | tar x -C /
        /kexec_nixos $@
        exit 1
        __ARCHIVE_BELOW__
      '';
    };
    kexec_bundle = pkgs.runCommand "kexec_bundle" { } ''
      cat \
        ${kexec_tarball_self_extract_script} \
        ${kexec_tarball}/tarball/nixos-system-${kexec_tarball.system}.tar \
> $out
      chmod +x $out
    '';
  };

 

  boot.initrd.availableKernelModules = [ "ata_piix" "uhci_hcd" ];
  boot.kernelParams = [
    "panic=30" "boot.panic_on_fail" # reboot the machine upon fatal boot issues
    "console=ttyS0" # enable serial console
    "console=tty1"
  ];
  boot.kernel.sysctl."vm.overcommit_memory" = "1";

 

  environment.systemPackages = with pkgs; [ cryptsetup btrfs-progs ];
  environment.variables.GC_INITIAL_HEAP_SIZE = "1M";

 

  networking.hostName = "kexec";

 

  services.getty.autologinUser = "root";
  services.openssh = {
    enable = true;
    settings = {
        KbdInteractiveAuthentication = false;
        PasswordAuthentication = false;
        };
  };

 

  documentation.enable = false;
  documentation.nixos.enable = false;
  fonts.fontconfig.enable = false;
  programs.bash.enableCompletion = false;
  programs.command-not-found.enable = false;
  security.polkit.enable = false;
  security.rtkit.enable = pkgs.lib.mkForce false;
  services.udisks2.enable = false;
  i18n.supportedLocales = [ (config.i18n.defaultLocale + "/UTF-8") ];

 

  users.users.root.openssh.authorizedKeys.keys = [
    # add your ssh key here
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQ....."
  ];

  system.stateVersion = "23.05";

}
EOF

 

# Here there must be only one channel without any special characters in the name, if there is, remove it and have only one there 
nix-channel --list
nix-channel --add https://nixos.org/channels/nixos-23.05 nixos
nix-channel --update

 

nix-build '<nixpkgs/nixos>' -A config.system.build.kexec_tarball -I nixos-config=./kexec.nix

 

cd /tmp && sudo tar xf /home/ubuntu/result/tarball/nixos-system-aarch64-linux.tar && sudo ./kexec_nixos

 

ssh root@somehost
